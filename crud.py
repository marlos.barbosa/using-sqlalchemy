from sqlalchemy.orm import Session
from fastapi import HTTPException

import models, schemas

#Professor

def get_professors(db:Session):
    return db.query(models.Professor).all()

def get_professor_by_id(db:Session,professor_id=int):
    db_professor = db.query(models.Professor).filter(models.Professor.professor_id==professor_id).first()
    if db_professor is None:
        raise HTTPException(status_code=400, detail='There is no professor with this ID')
    return db_professor

def get_professor_by_CPF(db:Session,CPF=int):
    return db.query(models.Professor).filter(models.Professor.CPF==CPF).first()

def create_professor(db:Session, professor: schemas.ProfessorBase):
    db_professor = get_professor_by_CPF(db,professor.CPF)
    if db_professor:
        raise HTTPException(status_code=400, detail='There is already a professor with this CPF')
    db_professor = models.Professor(**professor.dict())
    db.add(db_professor)
    db.commit()
    db.refresh(db_professor)
    return db_professor

#Course

def get_courses(db:Session):
    return db.query(models.Curso).all()

def get_course_by_id(db:Session, course_id:int):
    db_course = db.query(models.Curso).filter(models.Curso.course_id==course_id).first()
    if db_course is None:
        raise HTTPException(status_code=400, detail='There is no course with this ID')
    return db_course

def get_course_by_name(db:Session,course_name:str):
    return db.query(models.Curso).filter(models.Curso.course_name==course_name).first()

def create_course(db:Session,course:schemas.CursoBase):
    db_course = get_course_by_name(db,course.course_name)
    if db_course:
        raise HTTPException(status_code=400,detail='This course is already registered!')
    db_course = models.Curso(**course.dict())
    db.add(db_course)
    db.commit()
    db.refresh(db_course)
    return db_course

def assign_coordinator_to_course(db:Session, course_id: int, coordinator_id: int):
    course = get_course_by_id(db,course_id)
    professor = get_professor_by_id(db,coordinator_id)
    if professor is None or course is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    db.query(models.Curso).filter(models.Curso.course_id == course_id).update({"coordinator_id": coordinator_id})
    db.commit()
    db.refresh(course)
    return course

def get_students_of_course(db:Session,course_id:int):
    db_course = get_course_by_id(db,course_id)
    if db_course is None:
        raise HTTPException(status_code = 400, detail='There is no course with this ID')
    db_students = db.query(models.Aluno).filter(models.Aluno.course_id==course_id).all()
    return db_students

#Student

def get_students(db: Session):
    return db.query(models.Aluno).all()

def get_student_by_id(db:Session, student_id:int):
    db_student = db.query(models.Aluno).filter(models.Aluno.student_id==student_id).first()
    if db_student is None:
        raise HTTPException(status_code=400, detail='There is no student with this ID')
    return db_student

def get_student_by_CPF(db:Session,CPF:str):
    return db.query(models.Aluno).filter(models.Aluno.CPF==CPF).first()

def create_student(db:Session,student:schemas.AlunoBase):
    db_student = get_student_by_CPF(db,student.CPF)
    if db_student:
        raise HTTPException(status_code=400,detail='This student is already registered!')
    db_student = models.Aluno(**student.dict())
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return db_student

def assign_course_to_student(db:Session, student_id: int, course_id: int):
    student = get_student_by_id(db,student_id)
    course = get_course_by_id(db,course_id)
    if student is None or course is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    db.query(models.Aluno).filter(models.Aluno.student_id == student_id).update({"course_id": course_id})
    db.commit()
    db.refresh(student)
    return student

def enroll_student(db:Session,student_id:int,subject_id:int):
    student = get_student_by_id(db,student_id)
    subject = get_subject_by_id(db,subject_id)
    if student is None or subject is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    enrollment = check_enrollment(db,student_id,subject_id)
    if len(enrollment)!=0:
        raise HTTPException(status_code=400, detail='The student is already enrolled!')
    matricula = models.Matricula(student_id=student_id,subject_id=subject_id)
    db.add(matricula)
    db.commit()
    db.refresh(student)
    return student


def remove_student_from_subject(db:Session,student_id:int,subject_id:int):
    student = get_student_by_id(db,student_id)
    subject = get_subject_by_id(db,subject_id)
    if student is None or subject is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    check_enrollmentl=check_enrollment(db,student_id,subject_id)
    if len(check_enrollmentl)==0:
        raise HTTPException(status_code=400, detail='The student is not enrolled in this subject')
    db.query(models.Matricula).filter(models.Matricula.student_id==student_id).filter(models.Matricula.subject_id==subject_id).delete()
    db.commit()
    db.refresh(student)
    return student

def check_enrollment(db:Session,student_id:int,subject_id:int):
    return db.query(models.Matricula).filter(models.Matricula.student_id==student_id).filter(models.Matricula.subject_id==subject_id).all()

#Subject

def get_subjects(db: Session):
    return db.query(models.Disciplina).all()

def get_subject_by_id(db: Session, subject_id:int):
    db_subject = db.query(models.Disciplina).filter(models.Disciplina.subject_id==subject_id).first()
    if db_subject is None:
        raise HTTPException(status_code=400, detail='Subject does not exist yet')
    return db_subject

def get_subject_by_code(db:Session,code:str):
    return db.query(models.Disciplina).filter(models.Disciplina.code == code).first()

def create_subject(db:Session, subject:schemas.DisciplinaBase):
    db_subject = get_subject_by_code(db,subject.code)
    if db_subject:
        raise HTTPException(status_code=400,detail='The subject already exists')
    db_subject = models.Disciplina(**subject.dict())
    db.add(db_subject)
    db.commit()
    db.refresh(db_subject)
    return db_subject

def assign_professor_to_subject(db: Session, subject_id:int,professor_id:int):
    subject = get_subject_by_id(db,subject_id)
    professor = get_professor_by_id(db,professor_id)
    if subject is None or professor is None:
        raise HTTPException(status_code=400, detail='Provide different ID')
    db.query(models.Disciplina).filter(models.Disciplina.subject_id == subject_id).update({"professor_id": professor_id})
    db.commit()
    db.refresh(subject)
    return subject

def get_students_of_subject(db:Session,subject_id:int):
    db_subject = get_subject_by_id(db,subject_id)
    if db_subject is None:
        raise HTTPException(status_code=400, detail='Subject does not exist')
    return db.query(models.Aluno).join(models.Matricula).filter(models.Matricula.subject_id == subject_id).all()
