from pydantic import BaseModel, Field
from typing import List


def snake_to_camel(snake_str: str | None = None):
    if snake_str is None:
        return None
    splitted = snake_str.split('_')
    return splitted[0] + ''.join(s.title() for s in splitted[1:])


class GenericModel(BaseModel):
    class Config:
        alias_generator = snake_to_camel
        allow_population_by_field_name = True



class ProfessorBase(GenericModel):
    CPF: str = Field(title='CPF of professor', regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    professor_name: str = Field(title='Name of professor')
    professor_academic_degree: str = Field(title='Academic degree of professor')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'professor_name':'Wagner Pimentel',
                'CPF':'105.924.865-60',
                'professor_academic_degree':'Doutorado'
            }
        }

class Professor(ProfessorBase):
    professor_id: int = Field(title='ID of professor')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'professor_name':'Wagner Pimentel',
                'CPF':'105.924.865-60',
                'professor_academic_degree':'Doutorado',
                'professor_id':'4'
            }
        }

class ProfessorList(BaseModel):
    __root__: List[Professor]
    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of professors'
        }

class DisciplinaBase(GenericModel):
    subject_name: str = Field(title='Name of subject')
    code: str = Field(title='Code of subject')
    description: str = Field(title='Description of subject')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'subject_name':'Termodinâmica 2',
                'code':'EQUI-0103',
                'description':'The only interesting subject in the course so far.'
            }
        }

class Disciplina(DisciplinaBase):
    subject_id: int = Field(title='ID of subject')
    professor: Professor | None = Field(title = 'Professor of the subject')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'subject_name':'Termodinâmica 2',
                'code':'EQUI-0103',
                'description':'The only interesting subject in the course so far.',
                'subject_id':'4',
                'professor':{
                    'professor_name':'Wagner Pimentel',
                    'CPF':'105.924.865-60',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'4'
                }
            }
        }

class DisciplinaList(BaseModel):
    __root__: List[Disciplina]
    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of subjects'
        }

class CursoBase(GenericModel):
    course_name: str = Field(title='Name of course')
    creation_date: str = Field(title='Creation date of course')
    building_name: str= Field(title='Building name of course')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'course_name':'Engenharia Química',
                'creation_date':'26/01/2000',
                'building_name':'CTEC'
            }
        }


class Curso(CursoBase):
    course_id: int = Field(title='ID of course')
    coordinator: Professor | None = Field(title='Coordinator of course')
    class Config:
        orm_mode = True
        schema_extra={
            'example':{
                'course_name':'Termodinâmica 2',
                'creation_date':'EQUI-0103',
                'building_name':'The only interesting subject in the course so far.',
                'course_id':'4',
                'coordinator':{
                    'professor_name':'Wagner Pimentel',
                    'CPF':'105.924.865-60',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'4'
                }
            }
        }

class CursoList(BaseModel):
    __root__: List[Curso]
    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of courses'
        }

## Problema com data de aniversário ##
class AlunoBase(GenericModel):
    CPF: str = Field(title='CPF of student', regex='[0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[\-]?[0-9]{2}')
    student_name: str = Field(title='Name of student')
    birthday_date: str | None = Field(title='Birthday of student')
    RG: str = Field(title='RG of student')
    dispatching_agency: str = Field(title='Dispatching agency of student RG')
    class Config:
        orm_mode=True
        schema_extra = {
            'example': {
                'CPF': '123.369.521-74',
                'student_name': 'Marlos Ferreira Barbosa',
                'bithday_date':'26/02/2001',
                'RG':  '3852992-1',
                'dispatching_agency':'SEDS/AL'
            }
        }



class Aluno(AlunoBase):
    student_id: int = Field(title='ID of student')
    course: Curso | None = Field(title='Course of student')
    classes: DisciplinaList | None = Field(title='Classes of student')
    class Config:
        orm_mode=True
        schema_extra = {
            'example': {
                'CPF': '123.369.521-74',
                'student_name': 'Marlos Ferreira Barbosa',
                'bithday_date':'26/02/2001',
                'RG':  '3852992-1',
                'dispatching_agency':'SEDS/AL',
                'student_id': '7',
                'course': {
                    'course_name': 'Engenharia Química',
                    'creation_date': '26/01/2000',
                    'building_name': 'CTEC',
                    'course_id': '5',
                    'coordinator':{
                        'professor_name':'Wagner Pimentel',
                        'CPF':'105.924.865-60',
                        'professor_academic_degree':'Doutorado',
                        'professor_id':'4'
                    }
                },
                'classes': [
                    {
                'subject_name':'Termodinâmica 2',
                'code':'EQUI-0103',
                'description':'The only interesting subject in the course so far.',
                'subject_id':'4',
                'professor':{
                    'professor_name':'Wagner Pimentel',
                    'CPF':'105.924.865-60',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'4'
                        }
                    },
                    {
                'subject_name':'Termodinâmica 1',
                'code':'EQUI-0102',
                'description':'The worst subject in the course so far.',
                'subject_id':'5',
                'professor':{
                    'professor_name':'William',
                    'CPF':'105.924.865-80',
                    'professor_academic_degree':'Doutorado',
                    'professor_id':'5'
                    }
                    }
                ]
            }
        }



class AlunoList(BaseModel):
    __root__: List[Aluno]

    class Config:
        orm_mode=True
        arbitrary_types_allowed = True
        schema_extra = {
            'title': 'List of students'
        }
