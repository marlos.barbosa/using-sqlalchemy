from fastapi import Depends, FastAPI
from sqlalchemy.orm import Session
import uvicorn

import crud, schemas
from database import get_db

app = FastAPI(title="Hello FastAPI with SQLAlchemy")

#Tags
professor_tag = 'Professor'
course_tag= 'Course'
student_tag = 'Student'
subject_tag = 'Subject'

@app.get(
    '/professor',
    summary = 'Fetch all professors data',
    tags = [professor_tag],
    response_model=schemas.ProfessorList
)
def read_professors(db:Session = Depends(get_db)):
    return crud.get_professors(db)

@app.get(
    '/professor/{professor_id}',
    summary = 'Fetch one professor data',
    tags = [professor_tag],
    response_model=schemas.Professor
)
def read_professor(professor_id:int,db:Session = Depends(get_db)):
    return crud.get_professor_by_id(db,professor_id)

@app.post(
    '/professor/{professor_id}',
    summary = 'Create professor',
    tags = [professor_tag],
    response_model=schemas.Professor
)
def create_professor(professor: schemas.ProfessorBase, db:Session = Depends(get_db)):
    return crud.create_professor(db,professor)

@app.get(
    '/course',
    summary = 'Fetch all courses data',
    tags=[course_tag],
    response_model=schemas.CursoList
)
def read_courses(db:Session = Depends(get_db)):
    return crud.get_courses(db)

@app.get(
    '/course/{course_id}',
    summary = 'Fetch one course data',
    tags=[course_tag],
    response_model=schemas.Curso
)
def read_course(course_id:int,db:Session = Depends(get_db)):
    return crud.get_course_by_id(db,course_id)

@app.post(
    '/course',
    summary='Create new course',
    tags=[course_tag],
    response_model=schemas.Curso
)
def create_course(course:schemas.CursoBase,db: Session = Depends(get_db)):
    return crud.create_course(db,course)

@app.put(
    '/course/{course_id}/coordinator/{professor_id}',
    summary='Set coordinator to course',
    tags=[course_tag],
    response_model=schemas.Curso
)
def assing_coordinator_to_course(course_id:int,professor_id:int,db:Session = Depends(get_db)):
    return crud.assign_coordinator_to_course(db,course_id,professor_id)

@app.get(
    '/course/{course_id}/student',
    summary='Fetch students from a specific course',
    tags=[course_tag],
    response_model=schemas.AlunoList
)
def read_students_from_course(course_id: int, db: Session = Depends(get_db)):
    return crud.get_students_of_course(db,course_id)

@app.get(
    '/student',
    summary = 'Fetch all students data',
    tags=[student_tag],
    response_model=schemas.AlunoList
)
def read_students(db:Session = Depends(get_db)):
    return crud.get_students(db)

@app.get(
    '/student/{student_id}',
    summary = 'Fetch one student data',
    tags=[student_tag],
    response_model=schemas.Aluno
)
def read_student(student_id:int,db:Session = Depends(get_db)):
    return crud.get_student_by_id(db,student_id)

@app.post(
    '/student',
    summary='Create new student',
    tags=[student_tag],
    response_model=schemas.Aluno
)
def create_student(student:schemas.AlunoBase,db: Session = Depends(get_db)):
    return crud.create_student(db,student)

@app.put(
    '/student/{student_id}/course/{course_id}',
    summary='Set course to student',
    tags=[student_tag],
    response_model=schemas.Aluno
)
def assign_course_to_student(student_id:int,course_id:int,db:Session = Depends(get_db)):
    return crud.assign_course_to_student(db,student_id,course_id)

@app.post(
    '/student/{student_id}/subject/{subject_id}',
    summary = 'Enroll student in a class',
    tags = [student_tag],
    response_model=schemas.Aluno
)
def enroll_student(student_id:int,subject_id:int,db:Session=Depends(get_db)):
    return crud.enroll_student(db,student_id,subject_id)

@app.delete(
    '/student/{student_id}/subject/{subject_id}',
    summary='Unenroll student in a subject',
    tags=[student_tag],
    response_model=schemas.Aluno
)
def unenroll_student_in_class(student_id:int,subject_id:int,db:Session = Depends(get_db)):
    return crud.remove_student_from_subject(db,student_id,subject_id)

@app.get(
    '/subject',
    summary='Fetch all subjects data',
    tags = [subject_tag],
    response_model = schemas.DisciplinaList
)
def read_subjects(db:Session = Depends(get_db)):
    return crud.get_subjects(db)

@app.get(
    '/subject/{subject_id}',
    summary='Fetch one subject data',
    tags = [subject_tag],
    response_model = schemas.Disciplina
)
def read_subject(subject_id:int,db:Session = Depends(get_db)):
    return crud.get_subject_by_id(db,subject_id)

@app.post(
    '/subject',
    summary='Create new subject',
    tags = [subject_tag],
    response_model = schemas.Disciplina
)
def create_new_subject(subject: schemas.DisciplinaBase,db:Session = Depends(get_db)):
    return crud.create_subject(db,subject)

@app.put(
    '/subject/{subject_id}/professor/{professor_id}',
    summary='Assign a subject to a professor',
    tags = [subject_tag],
    response_model = schemas.Disciplina
)
def assign_subject(subject_id: int,professor_id:int,db:Session = Depends(get_db)):
    return crud.assign_professor_to_subject(db,subject_id,professor_id)

@app.get(
    '/subject/{subject_id}/student',
    summary='Fetch data of enrolled students',
    tags = [subject_tag],
    response_model = schemas.AlunoList
)
def read_subject(subject_id:int,db:Session = Depends(get_db)):
    return crud.get_students_of_subject(db,subject_id)



if __name__ == '__main__':
    uvicorn.run(app='main:app', port=3000, reload=True)
