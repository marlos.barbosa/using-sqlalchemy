from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

from database import Base


class Professor(Base):
    __tablename__ = "PROFESSORES"

    professor_id = Column(Integer, primary_key=True)
    CPF = Column(String, unique = True)
    professor_name = Column(String)
    professor_academic_degree = Column(String)

    course = relationship("Curso", back_populates = "coordinator")
    classes = relationship("Disciplina", back_populates="professor")



class Curso(Base):
    __tablename__ = "CURSOS"

    course_id = Column(Integer, primary_key=True,autoincrement=True)
    course_name = Column(String)
    creation_date = Column(String)
    building_name = Column(String)
    coordinator_id = Column(Integer, ForeignKey("PROFESSORES.professor_id"))

    coordinator = relationship("Professor", back_populates="course")
    students=relationship("Aluno", back_populates="course")


    

class Disciplina(Base):
    __tablename__ = "DISCIPLINAS"

    subject_id = Column(Integer, primary_key=True,autoincrement=True)
    subject_name = Column(String)
    code = Column(String)
    description = Column(String)
    professor_id = Column(Integer, ForeignKey("PROFESSORES.professor_id"))

    professor = relationship("Professor", back_populates="classes")
    student = relationship("Aluno", secondary='ALUNOS_DISCIPLINAS', back_populates="classes")

class Aluno(Base):
    __tablename__ = "ALUNOS"

    student_id = Column(Integer, primary_key=True,autoincrement=True)
    CPF = Column(String, unique = True)
    student_name = Column(String)
    birthday_date = Column(String)
    RG = Column(String)
    dispatching_agency = Column(String)
    course_id = Column(Integer, ForeignKey("CURSOS.course_id"))


    course = relationship("Curso",back_populates="students")
    classes = relationship("Disciplina", secondary='ALUNOS_DISCIPLINAS', back_populates = "student")


class Matricula(Base):
    __tablename__ = "ALUNOS_DISCIPLINAS"

    enrollment_id=Column(Integer, primary_key=True, autoincrement=True)

    subject_id = Column(Integer, ForeignKey("DISCIPLINAS.subject_id"))
    student_id = Column(Integer, ForeignKey("ALUNOS.student_id"))

    __table_args__ = (
        UniqueConstraint('subject_id','student_id'),
    )
